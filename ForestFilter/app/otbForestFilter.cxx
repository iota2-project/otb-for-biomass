/*=========================================================================

 Program:   ORFEO Toolbox
 Language:  C++
 Date:      $Date$
 Version:   $Revision$


 Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
 See OTBCopyright.txt for details.


 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "itkUnaryFunctorImageFilter.h"

namespace otb
{

namespace Functor 
{

template <typename TInputPixel, typename TOutputPixel>  class ForestDPFunctor
{
public:
  inline TOutputPixel operator()(const TInputPixel& in) const
  {

	  std::vector<float> vectPix;
	  float dryValue = 0;
	  
	  for (unsigned int i = 0; i < in.Size(); ++i)
	  {
		  if (in.GetElement(i) > 0.002)
		  {
		     vectPix.push_back(log10(in.GetElement(i)) * 10);
	      }
	  }
	  
	  if (vectPix.size() > 4)
	  {
		  sort(vectPix.begin(), vectPix.end());
		  //Compute the dry parameter
		  vectPix.resize(vectPix.size()/4);
		  for (unsigned int i = 0; i < vectPix.size(); ++i)
		  {
			  dryValue += vectPix[i];
		  }
		  dryValue /= vectPix.size();
	  } 
	  return dryValue;
  }

};

template <typename TInputPixel, typename TOutputPixel>  class ForestUAFunctor
{
public:
  inline TOutputPixel operator()(const TInputPixel& in) const
  {

	  // Recuperer le pixel vecteur dans vectPix
	  std::vector<float> vectPix;
	  float meanIntensity = 0.;
	  int compt = 0;
	  for (unsigned int i = 0; i < in.Size(); ++i)
	  {
		  if (in.GetElement(i) > 0.002)
		  {
		     ++compt;
		     meanIntensity += log10(in.GetElement(i)) * 10;
	      }
	  }
	  
	  if (compt > 4)
	  {
		  //Compute the mean of the SAR intensity
		  meanIntensity /= compt;
	  } else {
		  meanIntensity = 0.;
	  }
	  return meanIntensity;
  }

};


}

namespace Wrapper
{

class ForestFilter : public Application
{
public:
  /** Standard class typedefs. */
  typedef ForestFilter						   Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(ForestFilter, otb::Application);


  typedef otb::Functor::ForestDPFunctor<FloatVectorImageType::PixelType,FloatImageType::PixelType> ForestDPFunctorType;
  typedef itk::UnaryFunctorImageFilter<FloatVectorImageType,FloatImageType,ForestDPFunctorType> ForestDPFilterType;

  typedef otb::Functor::ForestUAFunctor<FloatVectorImageType::PixelType,FloatImageType::PixelType> ForestUAFunctorType;
  typedef itk::UnaryFunctorImageFilter<FloatVectorImageType,FloatImageType,ForestUAFunctorType> ForestUAFilterType;
  
private:
  void DoInit()
  {
    SetName("ForestFilter");
    SetDescription("");

    // Documentation
    SetDocName("ForestFilter");
    SetDocLongDescription("" );
						  
    SetDocLimitations("None");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso("SARPolarMatrixConvert, SARPolarSynth");

    AddDocTag(Tags::SAR);

    AddParameter(ParameterType_InputImage,  "in",   "Input Image");
    SetParameterDescription("in", "Input image");

    AddParameter(ParameterType_OutputImage, "outdry",  "Output dry parameter image");
    SetParameterDescription("outdry",  "Output dry parameter image");

    AddParameter(ParameterType_OutputImage, "outmeani",  "Output mean intensity image");
    SetParameterDescription("outmeani",  "Output mean intensity image");
    
    AddRAMParameter();

    // Default values
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {
    FloatVectorImageType::Pointer inputPtr = GetParameterFloatVectorImage("in");

    m_ForestDPFilter = ForestDPFilterType::New();
    m_ForestDPFilter->SetInput(inputPtr);

    SetParameterOutputImage("outdry", m_ForestDPFilter->GetOutput());


    m_ForestUAFilter = ForestUAFilterType::New();
    m_ForestUAFilter->SetInput(inputPtr);

    SetParameterOutputImage("outmeani",m_ForestUAFilter->GetOutput());
      
    
  }

  ForestDPFilterType::Pointer m_ForestDPFilter;
  ForestUAFilterType::Pointer m_ForestUAFilter;
  
  
}; 

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::ForestFilter)
