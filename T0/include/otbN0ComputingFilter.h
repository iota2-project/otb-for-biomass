/*=========================================================================

  Program:   ORFEO Toolbox
  Language:  C++
  Date:      $Date$
  Version:   $Revision$


  Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
  See OTBCopyright.txt for details.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __otbN0ComputingFilter_h
#define __otbN0ComputingFilter_h

#include "otbN0ComputingFunctor.h"
#include "otbUnaryFunctorImageFilter.h"

namespace otb
{

/** \class 
 * \brief 
 *
 * \ingroup IntensityImageFilters Multithreaded
 *
 * \ingroup 
 */

template <class TInputImage, class TOutputImage>
class ITK_EXPORT N0ComputingFilter :
  public UnaryFunctorImageFilter<
      TInputImage, TOutputImage,
      Functor::N0ComputingFunctor<
          typename TInputImage,
          typename TOutputImage::PixelType> >
{
public:
  /** Standard class typedefs. */
  typedef N0ComputingFilter Self;
  typedef otb::UnaryFunctorImageFilter<TInputImage, TOutputImage,
      Functor::N0ComputingFunctor<
          typename TInputImage,
          typename TOutputImage::PixelType>
      >  Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Macro defining the type*/



protected:
  N0ComputingFilter() {
}
  virtual ~N0ComputingFilter() {}
  virtual void GenerateOutputInformation()
	 {
		 // Call superclass implementation
		 Superclass::GenerateOutputInformation();
	 }
private:
  N0ComputingFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

};

} // end namespace otb

#endif
