/*=========================================================================

 Program:   ORFEO Toolbox
 Language:  C++
 Date:      $Date$
 Version:   $Revision$


 Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
 See OTBCopyright.txt for details.


 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "itkBinaryFunctorImageFilter.h"

namespace otb
{

namespace Functor 
{

template <typename TInputPixel,typename TInputPixelPOA, typename TOutputPixel>  class T33rotImageFunctor
{
public:
  inline TOutputPixel operator()(const TInputPixel& tMatrix,const TInputPixelPOA& POA) const
  {

    TOutputPixel outValue;
    //outValue.SetSize(1);
    outValue = static_cast<TOutputPixel>(0.5*(tMatrix[5]+tMatrix[3])+0.5*(tMatrix[5]-tMatrix[3])*cos(4.*POA)-tMatrix[4].real()*sin(4*POA));


    return outValue;
  }
};


}


namespace Wrapper
{

class T33rot : public Application
{
public:
  /** Standard class typedefs. */
  typedef T33rot   Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(T33rot, otb::Application);

typedef otb::Functor::T33rotImageFunctor<ComplexDoubleVectorImageType::PixelType,DoubleImageType::PixelType,ComplexDoubleImageType::PixelType> T33rotImageFunctor;

  typedef itk::BinaryFunctorImageFilter<ComplexDoubleVectorImageType,DoubleImageType,ComplexDoubleImageType,T33rotImageFunctor> T33rotImageFilterType;

  
private:
  void DoInit()
  {
    SetName("T33rot");
    SetDescription("");

    // Documentation
    SetDocName("T33rot");
    SetDocLongDescription("Rotate the T matrix and return the T33 component" );
    
						  
    SetDocLimitations("None");
    SetDocAuthors("Thierry Koleck (CNES)");
    SetDocSeeAlso("SARPolarMatrixConvert, SARPolarSynth");

    AddDocTag(Tags::SAR);

    AddParameter(ParameterType_ComplexInputImage,  "int",   "Input Image");
    SetParameterDescription("int", "Input image");
    AddParameter(ParameterType_InputImage,  "inpoa",   "Input Image");
    SetParameterDescription("inpoa", "Input image");

    AddParameter(ParameterType_ComplexOutputImage, "out",  "Output T33 image");
    SetParameterDescription("out", "Output T33 image");
    
    AddRAMParameter();

    // Default values
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {
    ComplexDoubleVectorImageType::Pointer inputPtrTMatrix = GetParameterComplexDoubleVectorImage("int");
    DoubleImageType::Pointer inputPtrPOA = GetParameterDoubleImage("inpoa");


    m_T33rotImageFilter = T33rotImageFilterType::New();
    m_T33rotImageFilter->SetInput1(inputPtrTMatrix);
    m_T33rotImageFilter->SetInput2(inputPtrPOA);

    SetParameterComplexOutputImage("out", m_T33rotImageFilter->GetOutput());
  }

  T33rotImageFilterType::Pointer m_T33rotImageFilter;  
  
}; 

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::T33rot)
