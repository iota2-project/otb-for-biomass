/*=========================================================================

 Program:   ORFEO Toolbox
 Language:  C++
 Date:      $Date$
 Version:   $Revision$


 Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
 See OTBCopyright.txt for details.


 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbN0ComputingFilter.h"

namespace otb
{
namespace Wrapper
{

class N0Computing : public Application
{
public:
  /** Standard class typedefs. */
  typedef N0Computing   Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(N0Computing, otb::Application);
  
  typedef otb::N0ComputingFilter<DoubleVectorImageType,DoubleImageType>DEMGeometryFilterType;
  
  
private:
  void DoInit()
  {
    SetName("N0Computing");
    SetDescription("");

    // Documentation
    SetDocName("N0Computing");
    SetDocLongDescription("" );
						  
    SetDocLimitations("None");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso("SARPolarMatrixConvert, SARPolarSynth");

    AddDocTag(Tags::SAR);
    
    AddParameter(ParameterType_InputImage,  "in",   "Normal image");
    SetParameterDescription("in", "Normal components image");

    AddParameter(ParameterType_String ,  "theta",   "Incidence Angle filename");
    SetParameterDescription("theta", "Incidence Angle filename");

    AddParameter(ParameterType_Double ,  "as",   "Pixel size");
    SetParameterDescription("as", "PixelSize in m2");
        
    AddParameter(ParameterType_OutputImage, "out",  "N0 image");
    SetParameterDescription("out", "Output N0 image");
   
    AddRAMParameter();

    // Default values
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {
    DoubleVectorImageType::Pointer inputPtr = GetParameterDoubleVectorImage("in");
    String ThetaFilename = GetParameterString("theta");
    double_t PixelSize = GetParameterDouble("as");

    //std::cout << "inputPtr1: " << inputPtr1 << std::endl;
    //std::cout << "inputPtr2: " << inputPtr2 << std::endl;

    m_N0ComputingFilter = DEMGeometryFilterType::New();
    m_N0ComputingFilter->SetInput(inputPtr);
    m_N0ComputingFilter->SetThetaFilename(ThetaFilename);
    m_N0ComputingFilter->SetPixelSize(PixelSize);
    

    SetParameterOutputImage("out", m_N0ComputingFilter->GetOutput());
  }

  DEMGeometryFilterType::Pointer m_N0ComputingFilter;
  
}; 

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::N0Computing)

