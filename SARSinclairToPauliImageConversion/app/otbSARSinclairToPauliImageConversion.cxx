/*=========================================================================

 Program:   ORFEO Toolbox
 Language:  C++
 Date:      $Date$
 Version:   $Revision$


 Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
 See OTBCopyright.txt for details.


 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbQuaternaryFunctorImageFilter.h"

namespace otb
{

namespace Functor 
{

template <typename TInputPixelHH,typename TInputPixelHV,typename TInputPixelVH,typename TInputPixelVV, typename TOutputPixel>  class SinclairToPauliImageFunctor
{
public:
  inline TOutputPixel operator()(const TInputPixelHH& inhh,const TInputPixelHV& inhv,const TInputPixelVH& invh,const TInputPixelVV& invv) const
  {

    TOutputPixel outValue;
    outValue.SetSize(4);
    outValue[0] = static_cast<typename TOutputPixel::ValueType>(1./sqrt(2.)*(inhh+invv));
    outValue[1] = static_cast<typename TOutputPixel::ValueType>(1./sqrt(2.)*(inhh-invv));
    outValue[2] = static_cast<typename TOutputPixel::ValueType>(1./sqrt(2.)*(inhv+invh));
    outValue[3] = static_cast<typename TOutputPixel::ValueType>(1./sqrt(2.)*(inhv-invh));

    return outValue;
  }
};


}

template <class TInputImageHH,class TInputImageHV,class TInputImageVH,class TInputImageVV,class TOutputImage, class TFunction= 
Functor::SinclairToPauliImageFunctor<typename TInputImageHH::PixelType, typename TInputImageHV::PixelType,typename TInputImageVH::PixelType,typename TInputImageVV::PixelType,typename TOutputImage::PixelType> >
class SinclairToPauliImageFilter : public otb::QuaternaryFunctorImageFilter<TInputImageHH,TInputImageHV,TInputImageVH,TInputImageVV, TOutputImage,TFunction >
 {
	 public:
		typedef SinclairToPauliImageFilter Self;
		typedef itk::SmartPointer<Self>        Pointer;
		typedef itk::SmartPointer<const Self>  ConstPointer;
		typedef otb::QuaternaryFunctorImageFilter<TInputImageHH,TInputImageHV,TInputImageVH,TInputImageVV,TOutputImage,TFunction> Superclass;
		
		itkNewMacro(Self);
		
	protected:
		SinclairToPauliImageFilter(){}
		virtual ~SinclairToPauliImageFilter(){}
	 
	 virtual void GenerateOutputInformation()
	 {
		 // Call superclass implementation
		 Superclass::GenerateOutputInformation();
		 
		 this->GetOutput()->SetNumberOfComponentsPerPixel(4);
	 }
	 
 };

namespace Wrapper
{

class SARSinclairToPauliImageConversion : public Application
{
public:
  /** Standard class typedefs. */
  typedef SARSinclairToPauliImageConversion   Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(SARSinclairToPauliImageConversion, otb::Application);

  typedef SinclairToPauliImageFilter<ComplexDoubleImageType,ComplexDoubleImageType,ComplexDoubleImageType,ComplexDoubleImageType,ComplexDoubleVectorImageType> SinclairToPauliImageFilterType;
  
private:
  void DoInit()
  {
    SetName("SARSinclairToPauliImageConversion");
    SetDescription("");

    // Documentation
    SetDocName("SARSinclairToPauliImageConversion");
    SetDocLongDescription("Convert the Sinclair components [Shh;Shv;Svh;Svv] to the Pauli components 1/sqrt(2)/[Shh+Svv;Shh-Svv;Shv+Svh;Shv-Svh]" );
    
						  
    SetDocLimitations("None");
    SetDocAuthors("Thierry Koleck (CNES)");
    SetDocSeeAlso("SARPolarMatrixConvert, SARPolarSynth");

    AddDocTag(Tags::SAR);

    AddParameter(ParameterType_ComplexInputImage,  "inhh",   "Input Image");
    SetParameterDescription("inhh", "Input image");
    AddParameter(ParameterType_ComplexInputImage,  "inhv",   "Input Image");
    SetParameterDescription("inhv", "Input image");
    AddParameter(ParameterType_ComplexInputImage,  "invh",   "Input Image");
    SetParameterDescription("invh", "Input image");
    AddParameter(ParameterType_ComplexInputImage,  "invv",   "Input Image");
    SetParameterDescription("invv", "Input image");

    AddParameter(ParameterType_ComplexOutputImage, "out",  "Output amplitude image");
    SetParameterDescription("out", "Output amplitude image");
    
    AddRAMParameter();

    // Default values
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {
    ComplexDoubleImageType::Pointer inputPtrHH = GetParameterComplexDoubleImage("inhh");
    ComplexDoubleImageType::Pointer inputPtrHV = GetParameterComplexDoubleImage("inhv");
    ComplexDoubleImageType::Pointer inputPtrVH = GetParameterComplexDoubleImage("invh");
    ComplexDoubleImageType::Pointer inputPtrVV = GetParameterComplexDoubleImage("invv");

    m_SinclairToPauliImageFilter = SinclairToPauliImageFilterType::New();
    m_SinclairToPauliImageFilter->SetInput1(inputPtrHH);
    m_SinclairToPauliImageFilter->SetInput2(inputPtrHV);
    m_SinclairToPauliImageFilter->SetInput3(inputPtrVH);
    m_SinclairToPauliImageFilter->SetInput4(inputPtrVV);

    SetParameterComplexOutputImage("out", m_SinclairToPauliImageFilter->GetOutput());
  }

  SinclairToPauliImageFilterType::Pointer m_SinclairToPauliImageFilter;  
  
}; 

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::SARSinclairToPauliImageConversion)
