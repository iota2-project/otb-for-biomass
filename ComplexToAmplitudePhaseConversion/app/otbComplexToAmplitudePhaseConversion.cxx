/*=========================================================================

 Program:   ORFEO Toolbox
 Language:  C++
 Date:      $Date$
 Version:   $Revision$


 Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
 See OTBCopyright.txt for details.


 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "itkUnaryFunctorImageFilter.h"

namespace otb
{

namespace Functor 
{

template <typename TInputPixel, typename TOutputPixel>  class VectorComplexToAmplitudeFunctor
{
public:
  inline TOutputPixel operator()(const TInputPixel& in) const
  {
    unsigned int nbComp = in.GetSize();

    TOutputPixel outValue(nbComp);
    
    for(unsigned int i = 0; i<nbComp;++i)
      {
      outValue[i] = static_cast<typename TOutputPixel::ValueType>(vcl_sqrt(in[i].real()*in[i].real()+in[i].imag()*in[i].imag()));
      }

    return outValue;
  }
};

template <typename TInputPixel, typename TOutputPixel>  class VectorComplexToPhaseFunctor
{
public:
  inline TOutputPixel operator()(const TInputPixel& in) const
  {
    unsigned int nbComp = in.GetSize();

    TOutputPixel outValue(nbComp);
    
    for(unsigned int i = 0; i<nbComp;++i)
      {
      outValue[i] = static_cast<typename TOutputPixel::ValueType>(vcl_atan2(in[i].imag(),in[i].real()));
      }

    return outValue;
  }
};


}

namespace Wrapper
{

class ComplexToAmplitudePhaseConversion : public Application
{
public:
  /** Standard class typedefs. */
  typedef ComplexToAmplitudePhaseConversion   Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(ComplexToAmplitudePhaseConversion, otb::Application);


  typedef otb::Functor::VectorComplexToAmplitudeFunctor<ComplexDoubleVectorImageType::PixelType,DoubleVectorImageType::PixelType> AmplitudeFunctorType;
  typedef itk::UnaryFunctorImageFilter<ComplexDoubleVectorImageType,DoubleVectorImageType,AmplitudeFunctorType> AmplitudeFilterType;

  typedef otb::Functor::VectorComplexToPhaseFunctor<ComplexDoubleVectorImageType::PixelType,DoubleVectorImageType::PixelType> PhaseFunctorType;
  typedef itk::UnaryFunctorImageFilter<ComplexDoubleVectorImageType,DoubleVectorImageType,PhaseFunctorType> PhaseFilterType;
  
private:
  void DoInit()
  {
    SetName("ComplexToAmplitudePhaseConversion");
    SetDescription("");

    // Documentation
    SetDocName("ComplexToAmplitudePhaseConversion");
    SetDocLongDescription("" );
						  
    SetDocLimitations("None");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso("SARPolarMatrixConvert, SARPolarSynth");

    AddDocTag(Tags::SAR);

    AddParameter(ParameterType_ComplexInputImage,  "in",   "Input Image");
    SetParameterDescription("in", "Input image");

    AddParameter(ParameterType_OutputImage, "outamp",  "Output amplitude image");
    SetParameterDescription("outamp", "Output amplitude image");

    AddParameter(ParameterType_OutputImage, "outphase",  "Output phase image");
    SetParameterDescription("outphase", "Output phase image");
    MandatoryOff("outphase");
    DisableParameter("outphase");
    
    AddRAMParameter();

    // Default values
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {
    ComplexDoubleVectorImageType::Pointer inputPtr = GetParameterComplexDoubleVectorImage("in");

    m_AmplitudeFilter = AmplitudeFilterType::New();
    m_AmplitudeFilter->SetInput(inputPtr);

    SetParameterOutputImage("outamp", m_AmplitudeFilter->GetOutput());

    if(IsParameterEnabled("outphase"))
      {
      m_PhaseFilter = PhaseFilterType::New();
      m_PhaseFilter->SetInput(inputPtr);

      SetParameterOutputImage("outphase",m_PhaseFilter->GetOutput());
      }
    
  }

  AmplitudeFilterType::Pointer m_AmplitudeFilter;
  PhaseFilterType::Pointer m_PhaseFilter;
  
  
}; 

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::ComplexToAmplitudePhaseConversion)
