/*=========================================================================

 Program:   ORFEO Toolbox
 Language:  C++
 Date:      $Date$
 Version:   $Revision$


 Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
 See OTBCopyright.txt for details.


 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "itkUnaryFunctorImageFilter.h"
#include "itkPowellOptimizer.h"
#include "otbPolarizationOptimalAngleFunctor.h"

namespace otb
{

namespace Wrapper
{

class CoherencyMatrixToPOA : public Application
{
public:
  /** Standard class typedefs. */
  typedef CoherencyMatrixToPOA          Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(CoherencyMatrixToPOA, otb::Application);


  typedef typename otb::Functor::VectorCoherencyMatrixToPOAFunctor<ComplexDoubleVectorImageType::PixelType,DoubleImageType::PixelType> VectorCoherencyMatrixToPOAFunctorType;
  typedef itk::UnaryFunctorImageFilter<ComplexDoubleVectorImageType,DoubleImageType,VectorCoherencyMatrixToPOAFunctorType> VectorCoherencyMatrixToPOAFilterType;

  
private:
  void DoInit()
  {
    SetName("CoherencyMatrixToPOA");
    SetDescription("");

    // Documentation
    SetDocName("CoherencyMatrixToPOA");
    SetDocLongDescription("" );
						  
    SetDocLimitations("None");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso("SARPolarMatrixConvert, SARPolarSynth");

    AddDocTag(Tags::SAR);

    AddParameter(ParameterType_ComplexInputImage,  "in",   "Input Image");
    SetParameterDescription("in", "Input image");

    AddParameter(ParameterType_OutputImage, "out",  "Output POA image");
    SetParameterDescription("out", "Output POA image");

    
    AddRAMParameter();

    // Default values
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {
    ComplexDoubleVectorImageType::Pointer inputPtr = GetParameterComplexDoubleVectorImage("in");

    m_POAFilter = VectorCoherencyMatrixToPOAFilterType::New();
    m_POAFilter->SetInput(inputPtr);

    SetParameterOutputImage("out", m_POAFilter->GetOutput());

    
  }

  VectorCoherencyMatrixToPOAFilterType::Pointer m_POAFilter;
  
  
}; 

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::CoherencyMatrixToPOA)
