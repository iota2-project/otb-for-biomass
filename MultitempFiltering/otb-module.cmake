set(DOCUMENTATION "Textures application (see also Textures module).")

otb_module(MultitempFiltering
  DEPENDS
    OTBCommon
    OTBITK
    OTBImageBase
    OTBApplicationEngine

  TEST_DEPENDS

  DESCRIPTION
    "${DOCUMENTATION}"
)
