/*=========================================================================

  Program:   ORFEO Toolbox
  Language:  C++
  Date:      $Date$
  Version:   $Revision$


  Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
  See OTBCopyright.txt for details.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef otbOutcoreFunctor_h
#define otbOutcoreFunctor_h
#include <iostream>

namespace otb
{

/** \class Functor::MeanRatio
 *
 * - compute the ratio of the two pixel values
 * - compute the value of the ratio of means
 * - cast the \c double value resulting to the pixel type of the output image
 * - store the casted value into the output image.
 *

 * \ingroup Functor
 *
 * \ingroup OTBChangeDetection
 */
namespace Functor
{

template<class TInput, class TOutput>
class OutcoreFunctor
{
public:
  OutcoreFunctor() {}
  virtual ~OutcoreFunctor() {}
  inline TOutput operator ()(const TInput& itA)
  {

    TOutput meanA;
    meanA.SetSize(1);
    meanA.Fill(0.);
    for (unsigned long pos = 0; pos < itA.Size(); ++pos)
      {
        meanA += itA.GetPixel(pos);
      }
    meanA /= itA.Size();
    TOutput ratio=itA.GetCenterPixel();
    ratio.SetSize(meanA.GetSize()+1);

    for (unsigned long i = 0; i < meanA.Size(); ++i)
       {
	   if (ratio[i]==0.)
	   {
		   ratio[i]=0.;
	   }
	   else
	   {
		   ratio[i]/=meanA[i];
	   }
       ratio[meanA.Size()]=int(meanA[0]>0.);
       }
    return static_cast<TOutput>(ratio);
  }
};
}
} // end namespace otb

#endif
