/*=========================================================================

  Program:   ORFEO Toolbox
  Language:  C++
  Date:      $Date$
  Version:   $Revision$


  Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
  See OTBCopyright.txt for details.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __otbDEMGeometryFilter_h
#define __otbDEMGeometryFilter_h

#include "otbDEMGeometryFunctor.h"
#include "otbUnaryFunctorNeighborhoodImageFilter.h"
#include "itkConstNeighborhoodIterator.h"
#include "otbDEMHandler.h"

namespace otb
{

/** \class 
 * \brief 
 *
 * \ingroup IntensityImageFilters Multithreaded
 *
 * \ingroup 
 */

template <class TInputImage, class TOutputImage>
class ITK_EXPORT DEMGeometryFilter :
  public UnaryFunctorNeighborhoodImageFilter<
      TInputImage, TOutputImage,
      Functor::DEMGeometryFunctor<
          typename itk::ConstNeighborhoodIterator<TInputImage>,
          typename TOutputImage::PixelType> >
{
public:
  /** Standard class typedefs. */
  typedef DEMGeometryFilter Self;
  typedef otb::UnaryFunctorNeighborhoodImageFilter<TInputImage, TOutputImage,
      Functor::DEMGeometryFunctor<
          typename itk::ConstNeighborhoodIterator<TInputImage>,
          typename TOutputImage::PixelType>
      >  Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  otb::DEMHandler::Pointer demHandler;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Macro defining the type*/



protected:
  DEMGeometryFilter() {
         demHandler = otb::DEMHandler::Instance();
         demHandler->OpenDEMDirectory("/data1/TropiSAR/mnt");
	 this->GetFunctor().SetDEM(demHandler);
}
  virtual ~DEMGeometryFilter() {}
  virtual void GenerateOutputInformation()
	 {
		 // Call superclass implementation
		 Superclass::GenerateOutputInformation();
		 
		 this->GetOutput()->SetNumberOfComponentsPerPixel(3);
	 }

   TreadedGenerateData(const OutputImageRegionType& outputRegionForThread,
                       itk::ThreadIdType threadId)
         {
  InputImagePointer  inputPtr = this->GetInput();
  OutputImagePointer outputPtr = this->GetOutput(0);

  // Define the portion of the input to walk for this thread, using
  // the CallCopyOutputRegionToInputRegion method allows for the input
  // and output images to be different dimensions
  InputImageRegionType inputRegionForThread;
  this->CallCopyOutputRegionToInputRegion(inputRegionForThread, outputRegionForThread);

  // Define the iterators
  itk::ImageRegionConstIterator<TInputImage>  inputIt(inputPtr, inputRegionForThread);
  itk::ImageRegionIterator<TOutputImage> outputIt(outputPtr, outputRegionForThread);

  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  inputIt.GoToBegin();
  outputIt.GoToBegin();
        while (!inputIt.IsAtEnd())
        {
        outputIt.Set(m_Gain * GetFunctor() (inputIt);
        ++inputIt;
        ++outputIt;
        progress.CompletedPixel(); // potential exception thrown here
        }
         }
private:
  DEMGeometryFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

};

} // end namespace otb

#endif
